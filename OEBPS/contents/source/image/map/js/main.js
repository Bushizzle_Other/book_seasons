function addOnWheel(elem, handler) {
    if ('onwheel' in document) elem.addEventListener("wheel", handler);
    else if ('onmousewheel' in document) elem.addEventListener("mousewheel", handler);
    else elem.addEventListener("MozMousePixelScroll", handler);
}

function throttle(func, ms) {

    var isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {

        if (isThrottled) { // (2)
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        func.apply(this, arguments); // (1)

        isThrottled = true;

        setTimeout(function() {
            isThrottled = false; // (3)
            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
}

$(function(){

    //$loadingOverlay.show();
    //$('.cp-button-container').hide();
    $header.add($text).add($check).add($restart).add($retry).remove();

    $('#close-media-object').text('Вернуться').appendTo($('.cp-button-container'));

    var resizer = new Image(),
        original = {
            width: 0,
            height: 0,
            ratio: 0
        };

    var $map = $('<div class="M__map">').appendTo($area),
        $mapContainer = $('<div class="M__map-container"></div>').appendTo($map),
        $mapLayers = $('<div class="M__map-layers"></div>').appendTo($mapContainer);

    // buttons

    var $btn_test = $('<div class="M__button M__button-test" title="Тест">'),
        $btn_info = $('<div class="M__button M__button-info" title="Информация">'),
        $btn_layers = $('<div class="M__button M__button-layers" title="Слои">'),
        $btn_legend = $('<div class="M__button M__button-legend" title="Условные обозначения">'),
        $btn_zoomIn = $('<div class="M__button M__button-zoom-in" title="Увеличить масштаб">'),
        $btn_zoomOut = $('<div class="M__button M__button-zoom-out" title="Уменьшить масштаб">');

    // buttons groups

    var $panel = $('<div class="M__panel">'),
        $controls = $('<div class="M__controls"></div>'),
        $zoom = $('<div class="M__zoom">'),
        $btn_position = $('<div class="M__button M__button-position">');

    // elements

    var $layersWrap = $('<div class="M__popup M__layers"><div class="M__popup-header">Слои</div></div>'),
        $layersList = $('<div class="M__popup-content M__layers-list">').appendTo($layersWrap);

    var $infoWrap = $('<div class="M__popup M__info"><div class="M__popup-header M__info-header">Информация</div></div>').appendTo($area),
        $infoFullscreen =
            $('<div class="M__popup-fullscreen-btn"></div>')
                .click(function(){
                    $infoWrap.toggleClass('M__popup_fullscreen')
                })
                .appendTo($infoWrap),
        $infoContent = $('<div class="M__popup-content M__info-content">').appendTo($infoWrap);

    var $testWrap = $('<div class="M__popup M__test"><div class="M__popup-header">Викторина</div>' +
        '<div class="M__popup-content M__test-content">' +
        '<h3>Вопрос ' + '<span class="M__test-num"></span>' + ' из ' + '<span class="M__test-sum"></span></h3>' +
        '<p class="M__test-text"></p>' +
        '<p class="M__test-task">Покажи один из значков на картие</p>' +
        '<div class="M__test-buttons">' +
        '<div class="M__test-button M__test-button-next">Следующий вопрос</div>' +
        '<div class="M__test-button M__test-button-end">Завершить викторину</div>' +
        '</div>' +
        '<div class="M__test-signal"></div>' +
        '</div>' +
        '<div class="M__test-result">' +
        '<div class="M__test-result-content"></div>' +
        '<div class="M__test-button M__test-button-restart">Покинуть викторину</div>' +
        '</div>' +
        '</div>').appendTo($area);

    var $testNum = $('.M__test-num'),
        $testSum = $('.M__test-sum'),
        $testText = $('.M__test-text'),
        $testNext = $('.M__test-button-next'),
        $testRestart = $('.M__test-button-restart'),
        $testEnd = $('.M__test-button-end'),
        $testContent = $('.M__test-content'),
        $testSignal = $('.M__test-signal'),
        $testResult = $('.M__test-result'),
        $testResultContent = $('.M__test-result-content');

    var test = {
        enabled: false,
        answered: false,
        current: 0,
        items: [],
        res: {
            correct: 0,
            wrong: 0,
            missed: 0
        },

        show: function(){
            resetPanel();
            this.enabled = true;
            $btn_test.add($testWrap).addClass('active');
        },
        hide: function(){
            resetPanel();
            this.enabled = false;
        },

        init: function(){
            $testSum.html(test.items.length);

            $testNum.html(1);
            $testText.html(this.items[0].text);

            $testNext.show();

        },

        signal: function(res, id){

            $testSignal
                .html(points[id].name + ' - ' + (res ? 'верный' : 'неверный') + ' ответ')
                .css('background', (res ? '#09FF34' : '#F12929'))
                .show();
        },

        toggle: function(){

            $testSignal.hide();

            this.current++;

            if(!this.answered) this.res.missed++;
            else this.answered = false;

            $testText.html(this.items[this.current].text);
            $testNum.html(this.current + 1);

            if(this.current + 1 == this.items.length) $testNext.hide();

        },

        result: function(){

            $testSignal.hide();

            if(this.current + 1 != this.items.length) {
                this.res.missed += (this.items.length - this.current - 1);
            }
            if(!this.answered) this.res.missed++;

            $testContent.hide();
            $testResult.show();
            $testResultContent
                .html(
                    '<div class="M__test-result-label">Верно</div>' +
                    '<div class="M__test-result-res">' + this.res.correct + '</div>' +
                    '<div class="M__test-result-label">Неверно</div>' +
                    '<div class="M__test-result-res">' + this.res.wrong + '</div>' +
                    '<div class="M__test-result-label">Пропущено</div>' +
                    '<div class="M__test-result-res">' + this.res.missed + '</div>'
                );
        },

        reset: function(){
            this.current = 0;
            this.answered = false;

            this.res.correct = 0;
            this.res.wrong = 0;
            this.res.missed = 0;

        },

        restart: function(){

            this.reset();

            $testContent.show();
            $testResult.hide();

        }
    };



    var layers = [],
        points = {},
        legend;

    var zoomLevel = 0, activePoint;

    loadLibs([
        ['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js']
    ]);

    loadXml(function(){

        $sandbox.append(xml);

        //$header.append($sandbox.find('title').html());
        //$text.append($sandbox.find('question').html());

        var $tasks = $sandbox.find('tasks');

        if($tasks.length){
            test.randomize = $sandbox.find('tasks').attr('randomize');
            test.reanswer = $sandbox.find('tasks').attr('allowreanswer');

            if(test.randomize == 'yes')$sandbox.find('task').shuffle();

            $sandbox.find('task').each(function(){

                test.items.push({
                    text: $(this).html(),
                    id: $(this).attr('answer') || '2242/'
                });

            });

            test.init();
        } else {
            $btn_test.hide();
            $controls.css('margin-top', 69);
        }

        $sandbox.find('layer').each(function(){
            var $el = $(this);
            layers.push({
                name: $el.html(),
                src: config.moduleFolder + 'layers/svg/' + $el.attr('src'),
                depth: $el.attr('depth')
            });
        });

        $sandbox.find('point').each(function(){
            var $el = $(this),
                id = $el.attr('id');

            points[id] = {
                name: $el.attr('name'),
                id: $el.attr('id'),
                img: config.contentFolder + $el.attr('img'),
                text: $el.find('text').html(),
                coords: {
                    x: $el.attr('x') - 20,
                    y: $el.attr('y') - 20
                }
            };
        });

        legend = config.contentFolder + $sandbox.find('legend').attr('src');

        resizer.onload = function(){
            original.width = $(resizer).width();
            original.height = $(resizer).height();
            original.ratio = original.width/original.height;

            $(resizer).addClass('M__resizer');

            layers.forEach(function(item, i){
                var $layer =$('<img class="M__layer" src="' + item.src + '" style="z-index:' + i + ';">');
                item.$el = $layer;
                $mapLayers.append($layer);
            });

            initTask();
        };

        $mapLayers.append(resizer);
        resizer.src = layers[0].src;

    });

    function initTask(){
        taskResizeHolder = resizeScene;
        $(window).trigger('resize');


        layers.forEach(function(item){
            $layersList.append(
                $('<div class="M__layers-list-item"><span>' + item.name + '</span></div>').click(function(){
                    $(this).toggleClass('disabled');
                    item.$el.toggle();
                })
            );
        });

        for(var k in points){

            if(points.hasOwnProperty(k)){

                var percentLeft = points[k].coords.x / (original.width/100),
                    percentTop = points[k].coords.y / (original.height/100);

                $mapLayers.append(
                    $('<div class="M__point"></div>')
                        .attr('id', points[k].id)
                        .css({
                            left: percentLeft + '%',
                            top: percentTop + '%'
                        })
                );

            }
        }

        $('.M__point').click(function(){

            if(test.enabled){

                if(!test.answered){
                    var id = $(this)[0].id;

                    if(id == test.items[test.current].id) {
                        test.signal(true, id);
                        test.res.correct++;
                    }
                    else {
                        test.signal(false, id);
                        test.res.wrong++;
                    }

                    test.answered = true;
                }

            } else {

                if(!$(this).hasClass('active')){
                    resetPanel();
                    $('.M__point').removeClass('active');
                    $(this).addClass('active');
                }

                setMapInfo($(this).attr('id'));
                showMapInfo($('.M__button-info'));

            }
        });


        // appending

        $panel.append(
            $controls.append($btn_test, $btn_info, $btn_layers, $btn_legend),
            $zoom.append($btn_zoomIn, $btn_zoomOut),
            $btn_position
        );

        $area.append($panel, $layersWrap);


        // EVENTS

        // panel

        $btn_layers.click(function(){

            if($(this).hasClass('active')){
                $layersWrap.removeClass('active');
            } else {
                resetPanel();

                $layersWrap.addClass('active');

            }

            $(this).toggleClass('active');
        });

        $btn_info.click(function(){

            if(activePoint) {

                if(!$(this).hasClass('active')){
                    resetPanel();
                    showMapInfo($(this));
                } else {
                    hideMapInfo($(this));
                }
            }

        });

        $btn_position.click(function(){
            $('body').toggleClass('panel-right');
            $(window).trigger('resize');
        });

        $btn_legend.click(function(){
            showFullscreenImage(legend);
            $btn_legend.addClass('active');
        });

        $('.overlay').click(function(){
            $btn_legend.removeClass('active');
        });

        $(window).resize(function(){
            if($btn_legend.hasClass('active'))$btn_legend.removeClass('active');
        });

        $btn_zoomIn.click(function(){mapZoom(10);});
        $btn_zoomOut.click(function(){mapZoom(-10);});

        $btn_test.click(function(){
            if(!test.enabled) test.show();
            else test.hide();
        });

        $testNext.click(function(){
            test.toggle();
        });

        $testEnd.click(function(){
            test.result();
        });

        $testRestart.click(function(){
            test.restart();
            test.hide();
        });

        // map

        $mapLayers.draggable({
            containment: "parent",
            scroll: false
        });

        addOnWheel($map[0], function(e){
            var delta = e.deltaY || e.detail || e.wheelDelta;

            if(delta < 0) mapZoom(10);
            else mapZoom(-10);
        });

        $loadingOverlay.hide();
    }

    function resetPanel(){
        $('.M__popup').add($('.M__button')).removeClass('active');
        if(test.enabled) test.enabled = false;
    }

    function setMapInfo(id){
        activePoint = points[id];

        $infoContent.html(
            '<h3>' + activePoint.name + '</h3>' +
            '<img class="M__popup-img" src="' + activePoint.img +'">' +
            activePoint.text);

        $infoContent.find('img').click(function(){
            showFullscreenImage(activePoint.img, activePoint.name);
        });
    }

    function showMapInfo($el){
        $infoWrap.addClass('active');

        if($el) $el.addClass('active');
    }

    function hideMapInfo($el){
        $infoWrap.removeClass('active');
        if($el) $el.removeClass('active');
    }

    function mapZoom(scale){
        if(zoomLevel + scale <= 100 && zoomLevel + scale >= 0){
            zoomLevel+=scale;

            $mapLayers.css({
                width: $map.width() /100*(100+zoomLevel),
                height: $map.height() /100*(100+zoomLevel)
            });

            if(zoomLevel) {
                $mapLayers.draggable('enable');

                var prevLeft = $mapLayers.parseCss('left'),
                    prevTop = $mapLayers.parseCss('top'),
                    direction = scale > 0 ? 1 : -1,
                    newLeft = prevLeft + direction*(prevLeft/10),
                    newTop = prevTop + direction*(prevTop/10);

                $mapContainer.css({
                    width: 100 + zoomLevel*2 + '%',
                    height: 100 + zoomLevel*2 + '%',
                    left: -zoomLevel + '%',
                    top: -zoomLevel + '%'
                });

                var leftOffset = $mapLayers.width() - $map.width(),
                    topOffset = $mapLayers.height() - $map.height();

                $mapLayers.css({
                    left: newLeft < leftOffset ? newLeft : leftOffset,
                    top: newTop < topOffset ? newTop : topOffset
                });
            }
            else if($mapLayers.hasClass("ui-draggable")) {
                $mapLayers
                    .draggable("disable")
                    .css({
                        left: 0,
                        top: 0
                    });
                $mapContainer.css({
                    width: '100%',
                    height: '100%',
                    left: 0,
                    top: 0
                });
            }

        }
    }

    var resizeScene = throttle(function(){
        var bottomOffset = $('.cp-button-container').outerHeight(),
            resWidth,
            resHeight,
            panelWidth = $('.M__panel').width(),
            areaWidth = window.innerWidth - panelWidth,
            areaHeight = window.innerHeight - bottomOffset - 1,
            areaRatio = areaWidth / areaHeight;

        $area.css({
            bottom: bottomOffset
        });

        if(original.ratio > areaRatio){

            resWidth = areaWidth;
            resHeight =  areaWidth/original.ratio

        } else {

            resWidth = areaHeight*original.ratio;
            resHeight =  areaHeight;

        }

        $map.css({width: resWidth, height: resHeight});
        $mapLayers.css({
            width: resWidth * (100 + zoomLevel)/100,
            height: resHeight * (100 + zoomLevel)/100
        });


    }, 50);




});